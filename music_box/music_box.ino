#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#include <NewPing.h>

#define MODE 2
#define PREVIOUS 4
#define NEXT 6
#define TRIGGER 7
#define ECHO 8
#define RGB_RED 9
#define RGB_GREEN 3
#define RGB_BLUE 5



#define MAX_DISTANCE 1500
#define MAX_SONG 60

enum mode {
  normal,
  randomness,
  plus_ten
};


int currentSong = 1;
bool paused = true;
mode mode_variable = normal;
int music_ended;

unsigned long time_initial = 0;

SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);
void RGB_color(int red_light_value, int green_light_value, int blue_light_value);

NewPing sonar(TRIGGER, ECHO, MAX_DISTANCE);

void setup()
{
  pinMode(MODE, INPUT);
  pinMode(PREVIOUS, INPUT);
  pinMode(NEXT, INPUT);
  pinMode(TRIGGER, OUTPUT);
  
  pinMode(RGB_RED, OUTPUT);
  pinMode(RGB_GREEN, OUTPUT);
  pinMode(RGB_BLUE, OUTPUT);
  
  mySoftwareSerial.begin(9600);
  Serial.begin(115200);

  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));

  myDFPlayer.volume(25);  //Set volume value. From 0 to 30
  myDFPlayer.play(currentSong);
  myDFPlayer.readState();
  music_ended = myDFPlayer.readState();
  myDFPlayer.pause();  //pause the mp3

  RGB_color(116, 255, 116); 
}

void loop()
{

  int distance = sonar.convert_in(sonar.ping_median(5));

  
  if( distance > 3 && paused) {
    myDFPlayer.start();  //start the mp3 from the pause
    paused = false;
  }
  else if((distance < 3 && distance != 0) && !paused) {
    myDFPlayer.pause();  //pause the mp3
    paused = true;
  }
  
  if(digitalRead(NEXT) == HIGH) {
    switch(mode_variable) {
      case normal:
        myDFPlayer.next();  //Play next mp3
        break;
      case randomness:
        myDFPlayer.play(1);
        delay(100);
        myDFPlayer.randomAll(); //Random play all the mp3.
        Serial.println(myDFPlayer.readState());
        break;
      case plus_ten:
        for(int i = 0; i < 10; i++) {
          myDFPlayer.next();
          delay(100);
        }
        break;
    }
    delay(500);
  }
  if(digitalRead(PREVIOUS) == HIGH) {
    switch(mode_variable) {
      case normal:
        myDFPlayer.previous();  //Play previous mp3
        break;
      case randomness:
        myDFPlayer.play(1);
        delay(100);
        myDFPlayer.randomAll(); //Random play all the mp3.
        Serial.println(myDFPlayer.readState());
        break;
      case plus_ten:
        for(int i = 0; i < 10; i++) {
          myDFPlayer.previous();
          delay(100);
        }
        break;
    }
    delay(500);
  }

  if(digitalRead(MODE) == HIGH) {
    switch(mode_variable) {
      case normal:
        mode_variable = plus_ten;
        RGB_color(0, 35, 255);
        delay(500);
        break;
      case randomness:
        mode_variable = normal;
        RGB_color(116, 255, 116);
        delay(500);
        break;
      case plus_ten:
        mode_variable = randomness;
        RGB_color(127, 255, 255);
        delay(500);
        break;
    }
  }

  // If a song has finished move on to the next automatically
  if(millis() - time_initial > 5000) {
    if(myDFPlayer.readState() == 512) {
      myDFPlayer.next();
      myDFPlayer.readState();
    }
    time_initial = millis();
  }
}

void RGB_color(int red_light_value, int green_light_value, int blue_light_value)
 {
  analogWrite(RGB_RED, red_light_value);
  analogWrite(RGB_GREEN, green_light_value);
  analogWrite(RGB_BLUE, blue_light_value);
}
